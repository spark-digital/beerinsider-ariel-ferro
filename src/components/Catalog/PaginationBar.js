import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { beersSelector } from "../../redux/beers/selectors";
import MESSAGES from '../../config/constants'
import { getBeersPaginationIndexes } from "../../functions";
import "./PaginationBar.css";

const PaginationBar = (props) => {
  const {
    currentPage,
    numberOfItems,
    numberOfPages,
    fromIndex,
    toIndex,
  } = props;

  const { searchTerm } = useSelector((state) => beersSelector(state));
  const { first, last, indexes } = getBeersPaginationIndexes(
    numberOfPages,
    currentPage
  );

  return (
    <section className="grid-pagination">
      <nav aria-label="Pagination links">
        {numberOfItems === 0
          ? MESSAGES.NO_BEERS_FOUND
          : `${MESSAGES.SHOWING} ${fromIndex + 1
          }-${toIndex} ${MESSAGES.OF} ${numberOfItems} beers found.`}
        <br />

        {first && (
          <>
            <Link to={`/?page=1&search=${encodeURIComponent(searchTerm)}`}>
              1
            </Link>{" "}
            <strong>...</strong>
          </>
        )}
        {indexes.map((page, index) => (
          <Link
            to={`/?page=${index + 1}&search=${encodeURIComponent(searchTerm)}`}
            key={index}
            aria-current={index + 1 === currentPage ? "page" : ""}
          >
            {index + 1}
          </Link>
        ))}
        {last && (
          <>
            <strong>...</strong>{" "}
            <Link
              to={`/?page=${numberOfPages}&search=${encodeURIComponent(
                searchTerm
              )}`}
            >
              {numberOfPages}
            </Link>
          </>
        )}
      </nav>
    </section>
  );
};

export default PaginationBar;
