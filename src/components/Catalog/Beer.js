import React from "react";
import { Link } from "react-router-dom";
import "./Beer.css";
import { getSummary } from "../../functions";

const Beer = (props) => {
  const { id, name, tagline, description, image_url } = props;

  return (
    <div className="grid-item">
      <Link to={`/beer/${id}`}>
        <img src={image_url} alt={name} />
        <h4>{name}</h4>
      </Link>
      <p>
        <strong>{tagline}</strong>
        <br />
        {getSummary(description)}
      </p>
    </div>
  );
};

export default Beer;
