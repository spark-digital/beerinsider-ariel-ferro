import React from "react";
import MESSAGES from '../config/constants';
import "./LoadingBox.css";

const LoadingBox = () => {
  return <div className="message-box message-box-}loading">{MESSAGES.LOADING}</div>;
};

export default LoadingBox;
