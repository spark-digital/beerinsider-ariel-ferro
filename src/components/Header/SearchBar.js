import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import Modal from "react-modal";
import { beersSelector } from "../../redux/beers/selectors";
import MESSAGES, { MODAL_STYLES } from "../../config/constants";
import FiltersBar from "./FiltersBar";
import "./SearchBar.css";

Modal.setAppElement("#root");

const SearchBar = () => {
  const history = useHistory();
  const { searchTerm, isLoading, hasError, filters } = useSelector((state) =>
    beersSelector(state)
  );

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const numberOfAppliedFilters = filters.reduce(
    (count, current) =>
      count + current.filters.filter((f) => f.isActive).length,
    0
  );

  const doSearch = (e) =>
    history.push("/?search=" + encodeURIComponent(e.target.value));

  const showFiltersBar = () => setModalIsOpen(true);

  return (
    <>
      <form onSubmit={(e) => e.preventDefault()}>
        <fieldset disabled={isLoading || hasError}>
          <input
            type="search"
            value={searchTerm}
            onInput={(e) => doSearch(e)}
            placeholder={MESSAGES.SEARCH}
            role="search"
          />
          <button
            type="button"
            onClick={(e) => showFiltersBar(e)}
            aria-label={`${MESSAGES.FILTER_BEERS} ${
              numberOfAppliedFilters
                ? `(${numberOfAppliedFilters} ${MESSAGES.ACTIVE_FILTERS})`
                : ""
            }`}
            className={`btn ${numberOfAppliedFilters ? "active" : ""}`}
          >
            <i className="fa fa-filter"></i>
          </button>
        </fieldset>
      </form>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={(e) => setModalIsOpen(false)}
        contentLabel={MESSAGES.FILTER_BEERS}
        style={MODAL_STYLES}
      >
        <FiltersBar onClose={(e) => setModalIsOpen(false)} />
      </Modal>
    </>
  );
};

export default SearchBar;
