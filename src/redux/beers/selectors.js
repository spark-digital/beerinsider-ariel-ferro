import { createSelector } from "reselect";
import { filterBeers } from "../../functions";

export const beersSelector = (state) => state.beer;

export const filteredBeersSelector = createSelector(beersSelector, (state) =>
  filterBeers(state.beers, state.filters, state.searchTerm)
);
