import {
  FETCH_BEERS_REQUEST,
  FETCH_BEERS_SUCCESS,
  FETCH_BEERS_FAILURE,
  FETCH_BEERS_ENDED,
  SET_SEARCH_TERM,
  SET_FILTERS,
} from "./types";
import beerFilters from "../../config/filters";
import * as cloneDeep from "lodash/cloneDeep";

const initialState = {
  isLoading: true,
  hasError: null,
  beers: [],
  searchTerm: "",
  filters: cloneDeep(beerFilters),
};

const beerReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_BEERS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_BEERS_SUCCESS:
      return {
        ...state,
        beers: state.beers.concat(action.payload),
        hasError: null,
      };
    case FETCH_BEERS_FAILURE:
      return {
        ...state,
        isLoading: false,
        beers: [],
        hasError: action.payload,
      };
    case FETCH_BEERS_ENDED:
      return {
        ...state,
        isLoading: false,
      };
    case SET_SEARCH_TERM:
      return {
        ...state,
        searchTerm: action.payload,
      };
    case SET_FILTERS:
      const { filters, groupIndex, filterIndex } = action.payload;
      const newFilters = [...filters];
      if (typeof groupIndex !== "undefined") {
        if (newFilters[groupIndex].multiple === false) {
          newFilters[groupIndex].filters.forEach(
            (filter) => (filter.isActive = false)
          );
        }
        const updatedFilter = newFilters[groupIndex].filters[filterIndex];
        updatedFilter.isActive = !updatedFilter.isActive;
      }
      return {
        ...state,
        filters: newFilters,
      };
    default:
      return state;
  }
};

export default beerReducer;
