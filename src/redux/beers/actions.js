import axios from "axios";
import {
  FETCH_BEERS_REQUEST,
  FETCH_BEERS_SUCCESS,
  FETCH_BEERS_FAILURE,
  FETCH_BEERS_ENDED,
  SET_SEARCH_TERM,
  SET_FILTERS,
} from "./types";

export const fetchBeers = (page = 1, setPage = () => {}) => {
  return (dispatch) => {
    dispatch(fetchBeersRequest());
    axios
      .get(`https://api.punkapi.com/v2/beers?per_page=80&page=${page}`)
      .then((response) => {
        dispatch(fetchBeersSuccess(response.data));
        if (response.data.length > 0) {
          setPage(page + 1);
        } else {
          dispatch(fetchBeersEnded());
        }
      })
      .catch((error) => {
        dispatch(fetchBeersFailure(error.message));
      });
  };
};

export const fetchBeersRequest = () => {
  return {
    type: FETCH_BEERS_REQUEST,
  };
};

export const fetchBeersSuccess = (beers) => {
  return {
    type: FETCH_BEERS_SUCCESS,
    payload: beers,
  };
};

export const fetchBeersFailure = (error) => {
  return {
    type: FETCH_BEERS_FAILURE,
    payload: error,
  };
};

export const fetchBeersEnded = () => {
  return {
    type: FETCH_BEERS_ENDED,
  };
};

export const setSearchTerm = (searchTerm) => {
  return {
    type: SET_SEARCH_TERM,
    payload: searchTerm,
  };
};

export const setFilters = (filters, groupIndex, filterIndex) => {
  return {
    type: SET_FILTERS,
    payload: { filters, groupIndex, filterIndex },
  };
};
