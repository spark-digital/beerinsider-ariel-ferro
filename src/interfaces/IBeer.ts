interface IBeer {
  id: number;
  name: string;
  tagline: string;
  first_brewed: string;
  description: string;
  image_url: string;
  abv: number; // Alcohol By Volume
  ibu: number; // International Biterness Unit - bitter value
  target_fg: number;
  target_og: number;
  ebc: number;
  srm: number; // Standard Reference Method - pale to dark colour 
  ph: number;
  attenuation_level: number;
  volume: Object;
  boil_volume: object;
  method: object;
  fermentation: object;
  ingredients: object;
  food_pairing: string[];
  brewers_tips: string;
  contributed_by: string;
}

export default IBeer;
