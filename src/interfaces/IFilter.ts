
export interface IFilter {    
label: string;
callbackFn: Function;
isActive: boolean;
}

export interface IFilterGroup {    
    label: string;
    multiple: boolean;
    filters: IFilter[];
    
}

