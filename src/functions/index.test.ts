import { findBeerById } from "./index";
import IBeer from "../interfaces/IBeer";

describe("function findBeerById", () => {
  const beers = [{ id: 1, name: "Beer name" } as IBeer];
  test("It should return the beer object inside of the beers array", () => {
    expect(findBeerById(beers, "1")).toHaveProperty("name");
  });
  test("It should return null when a beer with the given id does not exist inside the beers array", () => {
    expect(findBeerById(beers, "0")).toBeNull();
  });
});
